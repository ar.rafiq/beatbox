window.requestAnimFrame = (function (callback) { // shim
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();


function Character(options) {
    console.log("options", options);
    var that = {},
        frameIndex = 0,
        tickCount = 0,
        ticksPerFrame = options.ticksPerFrame || 0,
        numberOfFrames = options.numberOfFrames || 1;
    that.positionX = options.x || 0;
    that.positionY = options.y || 0;

    that.context = options.context;
    that.width = options.width;
    that.height = options.height;
    that.image = options.image;

    that.update = function () {

        tickCount += 1;

        if (tickCount > ticksPerFrame) {

            tickCount = 0;

            // If the current frame index is in range
            if (frameIndex < numberOfFrames - 1) {
                // Go to the next frame
                frameIndex += 1;
            } else {
                frameIndex = 0;
            }
        }
      
    };

    that.render = function () {
        // Clear the canvas
        that.context.clearRect(that.positionX, that.positionY, that.width, that.height);

        // Draw the animation
        that.context.drawImage(
            that.image,
            frameIndex * that.width / numberOfFrames,
            0,
            that.width / numberOfFrames,
            that.height,
            that.positionX,
            that.positionY,
            that.width / numberOfFrames,
            that.height);
    };

    return that;
}

function Playground() {

    const SECONDS = "8000";
    this.canvas = document.getElementById('playground');
    this.ctx = this.canvas.getContext('2d');
    this.preLoader = new PreLoader();
    var charactersInPlayground = [];
    var charactersObjCollection = [];
    this.timerId = null;
    this.init = function () {
        var ctx = this.ctx;
        this.ctx.font = "15px ";
        this.preLoader.setCollections(Collections);

        this.preLoader.onComplete(function (d) {
            console.log("recieved on complete");
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        });
        this.preLoader.onProgress(function (_data) {
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.fillText("LOADING " + Math.floor(_data.percentage) + "%", (ctx.canvas.width / 2) - 60, (ctx.canvas.height / 2) - 10);

        });

        this.preLoader.begin();
        
        animate();

    }
    this.addCharacter = function (characterData) {
        characterData.context=this.ctx;
        charactersInPlayground.push(characterData);
        console.log("adding char",characterData,charactersInPlayground);
        if (charactersInPlayground.length == 1) {
            this.refreshAudition();
            this.startTimer();
        }
    }

    this.refreshAudition = function () {
        var _charCollection = charactersInPlayground;
        var totalChar=charactersInPlayground.length;
       
        for (var _c = 0; _c < totalChar; _c++) {
            var _img = new Image();
            _img.src = _charCollection[_c].characters[_c].imgSrc;
            // console
                    console.log(Math.floor((60/100)*_charCollection[_c].characters[_c].frames),"SPEED");

            var _x=(totalChar==1)?0:totalChar*80;
            var _character = new Character({
                context:_charCollection[_c].context,
                width: _img.width,
                height: _img.height,
                image: _img,
                numberOfFrames: _charCollection[_c].characters[_c].frames,
                ticksPerFrame: Math.floor((60/100)*_charCollection[_c].characters[_c].frames),
                x:_x
            });
    if (!AudioContext) {
      new Audio(_charCollection[_c].audioSrc).play();
      return;
    }
            _charCollection[_c].audioBuffer.play();
            charactersObjCollection.push({obj:_character});

        }
       
    }
    
     var animate = function () { 
         requestAnimFrame(animate);
         if(charactersObjCollection.length>0){
             
            for (var _c = 0; _c < charactersObjCollection.length; _c++) {
                charactersObjCollection[_c].obj.render();
                charactersObjCollection[_c].obj.update();
            } 
         }
            
                
            }
            

    this.startTimer = function () {
        var _ref = this.refreshAudition;
        var _t=1;
        this.timerId = setInterval(function () {
            console.log("timer",_t++);
            _ref();
        }, SECONDS);
    }
    this.stopTimer = function () {
        clearInterval(this.timerId);
    }

    this.clearAll = function () {
        this.stopTimer();
        this.charactersInPlayground = [];
    }

}
