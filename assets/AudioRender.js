var AudioContext = window.AudioContext || window.webkitAudioContext || false;
var BufferEngine = new AudioContext();

const ON_PROGRESS="PROGRESS_EVENT";
const ON_RESOURCE_COMPLETED="RESOURCE_LOADED";
const ON_PROGRESS_EVENT=new CustomEvent(ON_PROGRESS,{detail:{index:0}});
const ON_RESOURCE_COMPLETED_EVENT=new CustomEvent(ON_RESOURCE_COMPLETED);

var Collections=[];
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, ],audioSrc:"/assets/sounds/melo1_toun_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/chips5_teylo_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/drum1_kick_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/chips3_yeah_a.mp3",audioBuffer:""});


Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/chips3_yeah_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/melo1_toun_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/effect1_bass_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/effect1_bass_a.mp3",audioBuffer:""});

Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/effect1_bass_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/drum1_kick_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/melo1_toun_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/effect1_bass_a.mp3",audioBuffer:""});

Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/drum1_kick_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/melo1_toun_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/drum1_kick_a.mp3",audioBuffer:""});
Collections.push({characters:[{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}, {imgSrc:"/assets/characters/beat_1.png",frames:22,imgBuffer:""},{imgSrc:"/assets/characters/sprites_boy.png",frames:8,imgBuffer:""}, {imgSrc:"/assets/characters/sprites_girl.png",frames:10,imgBuffer:""}],audioSrc:"/assets/sounds/effect1_bass_a.mp3",audioBuffer:""});

var loaded=0;
var totalResources=Collections.length+(Collections.length*5);//audio+images
var resNotLoaded=0;

function StreamAudio(url, data, cb) {
    var req = new XMLHttpRequest();
    data = (data) ? data : {};
    req.open('GET', url, true);
    req.responseType = 'arraybuffer';

    req.onload = function () {
        BufferEngine.decodeAudioData(req.response,
            function (buffer) {
                data.buffer = buffer;
                cb(data, false);
            },
            function (err) {
                data.error = err;
                cb(data, true);
            }
        );
    };
    req.send();
}

function PreLoader(ctx) {
    this.ctx = ctx;
    this.progressCb = null;
    this.setCollections = function (col) {
        Collections = col;
    }
    this.onProgress = function (cb) {
        this.progressCb = cb;
    }

    this.onComplete = function (cb) {
        window.addEventListener(ON_RESOURCE_COMPLETED, cb);
    }

    this.begin = function () {
        var _cb = this.progressCb;
        for (var i = 0; i < Collections.length; i++) {

            for (var j = 0; j < Collections[i].characters.length; j++) {
                var _imgBuff = new Image();
                _imgBuff.src = Collections[i].characters[j].imgSrc;
                _imgBuff.data = {
                    parent: i,
                    index: j
                };
                _imgBuff.onload = function (e) {
                    loaded++;
                    if (_cb) {
                        _cb({
                            totalResource: totalResources,
                            loadedResource: loaded,
                            percentage: (loaded / totalResources) * 100
                        });
                    }
                    Collections[this.data.parent].characters[this.data.index].imgBuffer = this;
                    window.dispatchEvent(ON_PROGRESS_EVENT);
                }

            }

            //AUDIO LOADING BELOW

            StreamAudio(Collections[i].audioSrc, {
                index: i
            }, function (res, isError) {
                if (!isError) {
                    Collections[res.index].audioBuffer = new AudioRender(BufferEngine).setBuffer(res.buffer);
                } else {
                    resNotLoaded++;
                }

                loaded++;
                if (_cb) {
                    _cb({
                        totalResource: totalResources,
                        loadedResource: loaded,
                        percentage: (loaded / totalResources) * 100
                    });
                }
            });


        }
    }

    window.addEventListener(ON_PROGRESS, function (e) {
        console.log("Loading", (loaded / totalResources) * 100);
        if ((loaded / totalResources) * 100 >= 96) {

            window.dispatchEvent(ON_RESOURCE_COMPLETED_EVENT);

        }
    });

}

function AudioRender(context, _buffer) {
    this.buffer = _buffer;
    var source = context.createBufferSource();
    if (_buffer)
        source.buffer = _buffer;

    source.loop = true;

    this.setBuffer = function (_buffer) {
        this.buffer = _buffer;
        source.buffer = this.buffer;

        return this;
    }
    this.play = function () {
        source = context.createBufferSource()
        source.buffer = this.buffer;
        source.connect(context.destination);

        source.start(0);
    };

    this.pause = function () {

        var source = context.createBufferSource();
        source.buffer = this.buffer;
        source.connect(context.destination);
        source.start();
    };

    this.stop = function () {
        source.stop();
    };

    return this;
}