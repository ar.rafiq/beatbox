/** 
 * @author Rafique Mohammed 
 **/
var beats_collections = [
    { character: 'aamir.png', sound: '/assets/sounds/effect1_bass_a.mp3' },
    { character: 'amitabh.png', sound: '/assets/sounds/melo2_flute_a.mp3' },
    { character: 'gabbar.png', sound: '/assets/sounds/drum2_snare_a.mp3' },
    { character: 'rajkapoor.png', sound: '/assets/sounds/effect3_cry_a.mp3' },
    { character: 'salman.png', sound: '/assets/sounds/effect1_bass_a.mp3' }    ];
var pitch_collections = [];
var currentTimeinMillis = 0;
var _timer;
var timerStarted = false;
var max_time_millis = 12 * 1000; //112
var _timer_event = new Event('timerCompeleted');;
$(document).ready(function () {
    var char_index = -1;
    var max_char = 4;
    var canvas = document.getElementById("compile");
    var ctx = canvas.getContext("2d");
    $(".add_char").on('click', function () {
        if (char_index < max_char) {
            char_index++;
        var _class = char_index + "_" + (new Date()).getMilliseconds();
            
            snapData(char_index,_class);
            $(".characters li").eq(char_index).find('img').fadeOut(300).attr("src",'/assets/characters/' + beats_collections[char_index].character).fadeIn(500);
        }

    });

$('.audition').on('click','.removeCharacter',function(){
    console.log("removed clicked");
    var _indx=$(this).data('index');
    console.log("closed index",_indx);
    $('.audio_'+pitch_collections[_indx].player).remove();
    $(".character_"+pitch_collections[_indx].player).remove();
    
    delete pitch_collections[_indx];
});
    $(".start-timer").on('click', function () {
        startTimer();
    });
    $(".stop-timer").on('click', function () {
        resetTimer();
    });
    $(".play").on('click', function () {
        startTimer();
    });
    //initial reset
    reset();
$('.audition').on('click','li',function(){
var sound = new Audio("/assets/sounds/amitabh.mp3");
    sound.preload = 'auto';
    sound.load();
    sound.play();
});
});
function reset() {
    $("#streamers").empty();
    currentTimeinMillis = 0;
}
function startTimer() {

    _timer = setInterval(function () {
        console.log("triggred interval");
        if(pitch_collections.length>0){
        document.dispatchEvent(_timer_event);
        }else{
            clearInterval(this);
        }
    }, 8000);
    if (!timerStarted) {
        timerStarted = true;
        looper();
    }

}
document.addEventListener('timerCompeleted', function (e) {
    console.log("event called");
    looper();
});
function resetTimer() {
    timerStarted = false;
    currentTimeinMillis = 0;
    clearInterval(_timer);
    $("#streamers").empty();

}
function getCurrentMillis() {
    return currentTimeinMillis;
}
function snapData(data_index,_cname) {
        var _class = "audio_"+_cname;

    pitch_collections.push({ index: data_index, player: _class, start_time: getCurrentMillis() });

    if (!timerStarted) {
        startTimer();
    }

}
function replayCollection() {

}
function looper() {
    $("#streamers").empty();
    if (pitch_collections.length > 0) {
        pitch_collections.forEach(function (value, index) {
            console.log("LOOPER.LOOP", value);
            $("#streamers").append("<audio autoplay class='audio_" + value.player + "' src=" + beats_collections[index].sound + " preload='auto'>");
        });
    }
}
