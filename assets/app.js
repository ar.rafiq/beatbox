var app = angular.module("playground", []);


app.config(function () {
    console.log("precache configuration");

});

app.service('Playground', Playground);

app.directive('canvas-control', {

});

app.controller('playCtrl', ['$scope', '$rootScope', 'Playground', function ($scope, $rs, Playground) {
    Playground.init();
    var recording = false;
    $scope.addCharacter = function (_in) {
        console.log(_in, Collections[_in]);
        Playground.addCharacter(Collections[_in]);
    }
    var canvas = document.querySelector('canvas');
    var recorder = new CanvasRecorder(canvas, {
        disableLogs: false
    });
    $scope.record = function () {
        if (!recording) {
            recording = !recording;
            // start recording <canvas> drawings
            recorder.record();


        } else {
            recording = !recording;
            stopRec();
            console.log("stopping");
        }
    }

    function stopRec() {
        // a few minutes later
        recorder.stop(function (blob) {
            var url = URL.createObjectURL(blob);
            console.log(url);
           // window.open(url);
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = url;
            a.download = "SpotBeats.mp4";
            a.click();
            window.URL.revokeObjectURL(url);
        });
    }

            }]);
